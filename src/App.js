import './App.css';
import StopWatch from './components/StopWatch';
import Hooks from './components/UsingHooks';
function App() {
  return (
    <div className="App">
      {/* <StopWatch></StopWatch> */}
      <Hooks></Hooks>
      
    </div>
  );
}

export default App;
