import React, { Component } from 'react';
import './style.css'

class StopWatch extends Component {

    constructor() {
        super()
        this.state = {
            counter : 0,
          }

        }
    
    runTimer  ()  {
        clearInterval(this.id);
        
       this.id =  setInterval(()=> {
            this.setState((preState) => ({
                counter : preState.counter +1
            }))
        },10);   //Delay time restriction 4ms
    }
    stopTimer() {
        clearInterval(this.id);
    }
    reset() {
        clearInterval(this.id);
        this.setState({
            counter : 0,
        });
    }
    getTime = (counter,divideValue,intoTime) =>{
       return ("0"+Math.floor((counter/divideValue)%intoTime)).slice(-2);
    }
render(){
    const { counter } = this.state;
    return (
        <div className='container'>
            <h1 className='heading'>{this.getTime(counter,360000,60)}:{this.getTime(counter,6000,60)}:
            {this.getTime(counter,100,60)}:{this.getTime(counter,1,100)}</h1>
            <button onClick={() => this.runTimer()} className="btn">Start</button>
            <button onClick={() => this.stopTimer()} className="btn">Stop</button>
            <button onClick={() => this.runTimer()} className="btn">Resume</button>
            <button onClick={() => this.reset()} className="btn">Reset</button>
        </div>

    )
}
}

export default StopWatch