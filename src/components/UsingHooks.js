import React, { useState,useRef } from "react";
import './style.css'

const Hooks = () => {
    const [count, setCount] = useState(0)
    const id = useRef(0) // it will return an object with property current

    const runTimer = () => {
        id.current && clearInterval(id.current)
         id.current = setInterval(() => {
            setCount(preCount => {
                return preCount + 1
            })
        }, 10)
    }

    const stopTimer = () => {
        id.current && clearInterval(id.current);
    }

    const reset = () => {
        id.current && clearInterval(id.current);
        setCount(preCount => {
            preCount = 0
            return preCount
        })
    }

    const getTime = (counter, divideValue, intoTime) => {
        return ("0" + Math.floor((counter / divideValue) % intoTime)).slice(-2);
    }

    return (
        <div className="container">
            <h1 className="heading">{getTime(count, 360000, 60)}:{getTime(count, 6000, 60)}:
                {getTime(count, 100, 60)}:{getTime(count, 1, 100)}</h1>
            <button className='btn' onClick={runTimer}>Start</button>
            <button className='btn' onClick={stopTimer}>Stop</button>
            <button className='btn' onClick={runTimer}>Resume</button>
            <button className='btn' onClick={reset}>Reset</button>
        </div>
    )
}

export default Hooks